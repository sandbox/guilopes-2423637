<?php

/**
 * @file
 * Administration functions for user_permisison_taxonomy.
 *
 * @author
 * Guilherme Lopes | guilopes
 */

/**
 * Form constructor for admin page.
 *
 * @return array
 *   Constructed form.
 *
 * @see user_permission_per_taxonomy_config_validate()
 */
function user_permission_per_taxonomy_config($form, &$form_state) {
  // Get all vocabularies.
  $vocabularies = taxonomy_get_vocabularies();

  $options = array();
  foreach ($vocabularies as $vocabulary) {
    $options[$vocabulary->vid] = $vocabulary->name;
  }

  $default = variable_get('user_permission_per_taxonomy_vid', array());
  // Permission per taxonomy.
  $form['user_permission_per_taxonomy_vid'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Vocabularies to set permissions'),
    '#options' => $options,
    '#default_value' => $default,
  );

  return system_settings_form($form);
}

/**
 * Validate callback.
 *
 * @see user_permission_per_taxonomy_config()
 */
function user_permission_per_taxonomy_config_validate($form, &$form_state) {
  $values = $form_state['values']['user_permission_per_taxonomy_vid'];

  foreach ($values as $vid => $value) {
    // Entity type and field name.
    $entity_type = 'taxonomy_term';
    $field_name = 'user_permission_per_taxonomy';

    // Load vocabulary.
    $vocabulary = taxonomy_vocabulary_load($vid);
    $machine_name = $vocabulary->machine_name;

    // Get instance field.
    $instance = field_info_instance($entity_type, $field_name, $machine_name);

    if (empty($value)) {
      if (!empty($instance)) {
        // Delete instance if user remove vocabulary.
        field_delete_instance($instance);
      }
      // Go to next.
      continue;
    }

    // Instance field.
    $instance = array(
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => $machine_name,
      'label' => t('User Permission List'),
    );

    try {
      // Create instance.
      field_create_instance($instance);
    }
    catch (FieldException $e) {
      $field = array(
        'field_name' => 'user_permission_per_taxonomy',
        'type' => 'list_text',
        'settings' => array(
          'allowed_values_function' => '_user_permission_per_taxonomy_get_options',
        ),
      );

      field_create_field($field);
      field_create_instance($instance);
    }

  }
}
