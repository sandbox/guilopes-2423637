<?php

/**
 * @file
 * Describe hooks provided by the User Permission per taxonomy module.
 */

/**
 * Implements hook_user_permission_per_taxonomy().
 *
 * This hook allow to user create your own permission.
 */
function hook_user_permission_per_taxonomy() {
  return array(
    'my permission name' => array(
      'title' => t('My Permission Name'),
    ),
  );
}

/**
 * Implements hook_user_permission_per_taxonomy_alter().
 *
 * Allow to user change others permissions set.
 */
function hook_user_permission_per_taxonomy_alter(&$permissions) {

}
